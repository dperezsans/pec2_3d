# PEC1 3D 

## Introducción

Se solicita la creación de un juego FPS al estilo Shooter.

## Terreno

El terreno está diseñado en un entorno monatañoso ,con mucho desnivel. Además se ha incorporado un entorno cerrado , como si fuera una base.

Respecto a los terrenos, se han utilizado los siguientes elementos:

### Paint Textures
Se han utilizado las siguientes texturas:
- Floor
- Rocks
- Grass
- Snow

### Paint Trees

Referente a los tipos de árboles se ha añadido un tipo de árbol córtado. 

## Entorno carretera

Para el desarrollo del circuito, se ha optado por utilizar el paquete EasyRoad3D Free. Además, todo el circuito está colocado en la capa road con la etiqueta Road.

### Obstáculos y señales

Para dar mejor aspecto y jugabilidad, se han icorporado elementos con collider dentro del juego, como puede ser; barreras, señales o banderas de meta.


## FPS

El juego está orientado a una jugabilidad en primera persona. Para poder realizar este efecto, la cámara MainCamara está asociada al jugador, justo en la parte frontal del mismo.

Respecto al código utilizado para la gestión básica del jugador, se ha usado un starter pack de Unity Assets.

## Armas

El control de las armas se realiza mediante el script WeaponController, pudiendo gestionas las características:

- Shot Type: automático o manual.
- Recoil Force: Retroceso del arma.
- Fire Range: Rango del arma al disparar.
- Buller Hole: Efecto de disparo en algun collider.
- Fire Rate: Velocidad del arma.
- Max Ammo: Cantidad de balas en el cargador.
- Total Ammo: Cantidad de balas disponibles.

Para el juego se ha incorporado tres armas; una ametralladora, una pistola y un rifle.

## UI

Respecto al UI, se proporciona información de las balas de cada arma y la vida del jugador.


## Elemento movibles

### Puertas

Existe dos puerta en la base que se puede abrir usando la tecla E.

### Elevador

Para poder finalizar la escena, es necesario subir a una planicie elevada. Solo se puede acceder si se sube al ascensor con movimiento vertical.

### Transportador

Otro elemento movible es Trasnportador de movimiento horizontal, usuado para solventar un barranco.

## DeathZone

El juego cuenta con dos DeathZone. El primero es en el barranco de dentro de la base. El otro es la parte inferior del elevador.


## Controles

- El desplazamiento se realiza don las teclas "W", "S", "A" y "D".
- Acciones de abrir puertas, "E".
- Correr "SHIFT".
- Selección de armas "1", "2" y "3".
- Salto "space".
- Disparo "Click izquierdo del ratón".
- Recargar arma "R".




