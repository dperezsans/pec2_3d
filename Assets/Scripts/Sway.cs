using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sway : MonoBehaviour
{

    private Quaternion originalLocalRotation;
    public float forceSway = 1.45f;

    // Start is called before the first frame update
    void Start()
    {
        originalLocalRotation = transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        updateSway();
    }

    private void updateSway() {

        float t_xLookInput = Input.GetAxis("Mouse X");
        float t_yLookInput = Input.GetAxis("Mouse Y");

        Quaternion t_xAngleAdjustment = Quaternion.AngleAxis(-t_xLookInput * forceSway, Vector3.up);
        Quaternion t_yAngleAdjustment = Quaternion.AngleAxis(-t_yLookInput * forceSway, Vector3.right);
        Quaternion t_targerRotation = originalLocalRotation * t_xAngleAdjustment * t_yAngleAdjustment;

        transform.localRotation = Quaternion.Lerp(transform.localRotation, t_targerRotation, Time.deltaTime * 10f);
    }
}
