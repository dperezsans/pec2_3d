using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponController : MonoBehaviour
{
    public List<WeaponController> startingWeapons = new List<WeaponController>();

    public Transform weaponParentSocket;
    public Transform defautlParentSocket;
    public Transform aimParentSocket;

    public int activeWeaponIndex { get; private set; }

    private WeaponController[] weaponSlots = new WeaponController[3];

    // Start is called before the first frame update
    void Start()
    {
        activeWeaponIndex = -1;
        int index = 0;
        foreach (WeaponController startingWeapon in startingWeapons) {
            
            AddWeapon(startingWeapon, index);
            index++;
        }
        switchWeapon(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1)){

            switchWeapon(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {

            switchWeapon(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {

            switchWeapon(2);
        }

    }

    private void switchWeapon(int index) {

        Vector3 effectSwitch = new Vector3(0.155f, -0.27f, 0.499f);


        if (weaponSlots[index] != null) {
            for (int i = 0; i < weaponSlots.Length; i++)
            {
                if(weaponSlots[i] != null)
                    weaponSlots[i].gameObject.SetActive(false);
            }

            
            weaponSlots[index].gameObject.SetActive(true);
            activeWeaponIndex = index;

            
           
                
             weaponSlots[index].transform.position = defautlParentSocket.position + effectSwitch;
          
            
            EventManager.current.NewGunEvent.Invoke();
            
        }
       

        
    }

    private void AddWeapon(WeaponController p_weaponPrefab, int index) { 
    
        weaponParentSocket.position = defautlParentSocket.position;

        if (weaponSlots[index] == null) {
            WeaponController weaponClone = Instantiate(p_weaponPrefab, weaponParentSocket);
            weaponClone.owner = gameObject;
            weaponClone.gameObject.SetActive(false);

            weaponSlots[index] = weaponClone;
               
        }
        
    }
}
