using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class HUDManager : MonoBehaviour
{
    public GameObject weaponInfoPrefab;
    public GameObject finishGamePrefab;

    private void Start() {

        EventManager.current.NewGunEvent.AddListener(CreateWeaponInfo);
        EventManager.current.YouWin.AddListener(FinishGame);
        EventManager.current.YouLoose.AddListener(FinishGame);
    }

    public void CreateWeaponInfo() { 
    
        Instantiate(weaponInfoPrefab, transform);
    }

    public void FinishGame()
    {


        weaponInfoPrefab.SetActive(false);
        Instantiate(finishGamePrefab, transform);
    }
}
