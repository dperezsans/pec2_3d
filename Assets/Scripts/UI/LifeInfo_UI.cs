using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LifeInfo_UI : MonoBehaviour
{
    public TMP_Text currentLife;
    public TMP_Text currentArmor;



    public void UpdateLife(int newCurrentLife, int newCurrentArmor)
    {

        if (newCurrentLife <= 0)
        {
            currentLife.color = Color.red;

        }
        else
        {
            currentLife.color = Color.white;
        }
        currentLife.text = newCurrentLife.ToString();
        currentArmor.text = newCurrentArmor.ToString();
    }

    private void OnEnable()
    {
        EventManager.current.updateLifeEvent.AddListener(UpdateLife);
    }

    private void OnDisable()
    {
        EventManager.current.updateLifeEvent.RemoveListener(UpdateLife);
    }
}
