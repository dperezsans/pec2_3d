using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WeaponInfo_UI : MonoBehaviour
{
    public TMP_Text currentBullets;
    public TMP_Text totalBullets;

    

    public void UpdateBullets(int newCurrentBullet, int newTotalBullet) 
    {

        if (newCurrentBullet <= 0)
        {
            currentBullets.color = Color.red;

        }
        else {
            currentBullets.color = Color.white;
        }
        currentBullets.text = newCurrentBullet.ToString();
        totalBullets.text = newTotalBullet.ToString();
    }

    private void OnEnable()
    {
        EventManager.current.updateBulletsEvent.AddListener(UpdateBullets);
    }

    private void OnDisable() {
        EventManager.current.updateBulletsEvent.RemoveListener(UpdateBullets);
    }
}
