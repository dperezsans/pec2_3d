using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ActionOpenDoorController : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] public int startPoint;
    [SerializeField] public Transform[] points;


    private int i;
    public bool reverse;
    private int actioned = 0;

    void Start()
    {
        transform.position = points[startPoint].position;
        i = startPoint;
    }

    private void Update()
    {
        if (actioned == 1)
        {

            transform.position = Vector3.MoveTowards(transform.position, points[1].position, speed * Time.deltaTime);

           
        }

       
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player") {

            if (Input.GetKeyDown(KeyCode.E) && actioned == 0) {
               
                actioned = 1;
                
            }
        }
    }
}
