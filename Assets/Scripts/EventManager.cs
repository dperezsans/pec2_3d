using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class DuplaIntEvent : UnityEvent<int, int> { }


public class EventManager : MonoBehaviour
{
    #region Singleton
    public static EventManager current;
    public void Awake()
    {
        if (current == null) { current = this; } else if (current != null) { Destroy(this); }

    }
    #endregion


    public DuplaIntEvent updateBulletsEvent = new DuplaIntEvent();
    
    public DuplaIntEvent updateLifeEvent = new DuplaIntEvent();
    public UnityEvent NewGunEvent = new UnityEvent();
   public UnityEvent YouWin = new UnityEvent();
    public UnityEvent YouLoose = new UnityEvent();

}
