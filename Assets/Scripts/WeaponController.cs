using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.Rendering;
using static UnityEngine.UI.Image;

public enum ShotType
{ 
    Manual,
    Automatic
}

public class WeaponController : MonoBehaviour
{
    [Header("References")]
    public Transform weaponMuzzle;


    [Header("General")]
    public LayerMask hitableLayers;


    [Header("Shoot Parameter")]
    public ShotType shotType;
    public float recoilForce = 4f;
    public float fireRange = 200;
    public GameObject bulletHolePrefab;
    public float fireRate = 0.6f;
    public int maxAmmo = 8;
    public int totalAmmo = 120;

    [Header("Reload Parameters")]
    public float reloadTime = 1.5f;

    public int currentAmmo { get; private set; }

    [Header("Multimedia")]
    public GameObject flashEffects;

    public GameObject owner { set; get; }

    private Transform cameraPlayerTransform;
    private float lastTimeShoot = Mathf.NegativeInfinity;

    private int enJuego = 1;


    private void Awake()
    {
        currentAmmo = maxAmmo;
        EventManager.current.updateBulletsEvent.Invoke(currentAmmo,maxAmmo);
    }

    private void Start()
    {
        cameraPlayerTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
        //EventManager.current.YouWin.AddListener(FinishGame);
        //EventManager.current.YouLoose.AddListener(FinishGame);
    }

    private void FinishGame()
    {
        enJuego = 0;
    }

    private void Update()
    {
        if (enJuego == 1)
        {
            if (shotType == ShotType.Manual)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    TryShoot();
                }
            }
            else if (shotType == ShotType.Automatic)
            {
                if (Input.GetButton("Fire1"))
                {
                    TryShoot();
                }

            }


            if (Input.GetKeyDown(KeyCode.R))
            {
                StartCoroutine(Reload());
            }

            transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, Time.deltaTime * 5f);
        }
        
    }

    private bool TryShoot() {

        if (lastTimeShoot + fireRate < Time.time) {

            if (currentAmmo >= 1) {
                HandleShoot();
                currentAmmo -= 1;
                EventManager.current.updateBulletsEvent.Invoke(currentAmmo, maxAmmo);
                return true;
            }
        }

        return false;
    }

    private void HandleShoot() {

       
        Instantiate(flashEffects, weaponMuzzle.position, Quaternion.Euler(weaponMuzzle.forward), transform);

        AddRecoil();
       
       

        RaycastHit[] hits;
        hits = Physics.RaycastAll(cameraPlayerTransform.position, cameraPlayerTransform.forward, fireRange, hitableLayers);

        foreach (RaycastHit hit in hits) {
            if (hit.collider.gameObject != owner) {
                GameObject bulletHoleClone = Instantiate(bulletHolePrefab, hit.point + hit.normal * 0.001f, Quaternion.LookRotation(hit.normal));
                Destroy(bulletHoleClone, 4);
                Debug.Log("Disparo");
            }
        }

        lastTimeShoot = Time.time;
    }

    private void AddRecoil() {
        transform.Rotate(-recoilForce, 0f, 0f);
        transform.position = transform.position - transform.forward * (recoilForce / 50f);
    }

    IEnumerator Reload() {

        if (totalAmmo > 0)
        {
            Debug.Log("Recargando...");
            Vector3 effectReload = new Vector3(-0.5f, -2f, -0.5f);
            transform.localPosition = Vector3.Lerp(transform.localPosition + effectReload, Vector3.zero, Time.deltaTime * 5f);
            yield return new WaitForSeconds(reloadTime);
            if (totalAmmo > maxAmmo)
            {
                totalAmmo = currentAmmo - maxAmmo;
                currentAmmo = maxAmmo;
                
            }
            else {
                currentAmmo = totalAmmo;
                totalAmmo = 0;
            }
            
            
            EventManager.current.updateBulletsEvent.Invoke(currentAmmo, totalAmmo);
            Debug.Log("Recargado...");
        }
        else { 
        }
        
    }
}
